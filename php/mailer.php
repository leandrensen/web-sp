<?php
	use PHPMailer\PHPMailer\PHPMailer;

	//SMTP needs accurate times, and the PHP time zone MUST be set
	//This should be done in your php.ini, but this is how to do it if you don't have access to that
	date_default_timezone_set('America/Argentina/Buenos_Aires');

	require '../vendor/autoload.php';	

	$name = $_POST['name'];
	$email = $_POST['email'];
	$telephone = $_POST['telephone'];
	$comment = $_POST['comment'];

	$mail = new PHPMailer(); // create a new object
	$mail->IsSMTP(); // enable SMTP
	$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = true; // authentication enabled
	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
	$mail->Host = "smtp.gmail.com";
	$mail->Port = 465; // or 587
	$mail->IsHTML(true);

	// Authentication
	$mail->Username = "lmendez89@gmail.com";
	$mail->Password = "821963249";

	$mail->SetFrom($email, $name); //Set who the message is to be sent from
	$mail->Subject = "Aviso de Nuevo Contacto"; //Set the subject line
	$mail->Body = 'Nombre: ' . $name . '<br>' .
				  'E-Mail: ' . $email . '<br>' .
				  'Telefono: ' . $telephone . '<br>' .
				  'Mensaje: ' . $comment;
	$mail->AddAddress("lmendez89@gmail.com"); //Set who the message is to be sent to

	if(!$mail->Send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
		header("Location: http://spserviciostdf.com/contacto.html");
		die();
	}